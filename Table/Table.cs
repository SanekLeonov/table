﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Table
{
    public class Table
    {
        public static void Show()
        {
            var collectionNumbers = new List<Numbers>();

            Console.WriteLine("Задайте шаг построения X");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.Write("Задайте диапазон Х в формате number:numberTwo \n");
            string input = Console.ReadLine();

            string pattern = @"^([0-9]+):([0-9]+)$";
            var matches = Regex.Matches(input, pattern);

            int startRange = 0, endRange = 0;

            foreach (Match match in matches)
            {
                startRange = Convert.ToInt32(match.Groups[1].Value);
                endRange = Convert.ToInt32(match.Groups[2].Value);
            }

            for (int i = startRange; i < endRange; i += x)
            {
                var number = new Numbers
                {
                    x = i,
                    y = GetY(i)
                };

                collectionNumbers.Add(number);
            }

            var lastElement = collectionNumbers[collectionNumbers.Count - 1]; //костыль (хотел получить последний элемент в коллекции, надо глянуть как можно это сделать)
            var numberCharsInValue = GetNumberChairsInValue(lastElement.y); //после того как получил последний элемент в коллекции надо получить количество символов в нем

            foreach (var number in collectionNumbers)
            {                
                var numberSpaces = numberCharsInValue - GetNumberChairsInValue(number.y); //узнать сколько пробелов напечатать
                var strSpaces = PrintWhiteSpace(numberSpaces); //напечатанные пробелы в строке                
                Console.WriteLine($"| {number.x}{strSpaces} | {strSpaces}{number.y} |");
            }
        }

        public static int GetY(int x)
        {
            return x + 1;
        }

        struct Numbers
        {
            public int x;
            public int y;
        }

        static int GetNumberChairsInValue(int value)
        {
            return (int)Math.Log10(value) + 1; //получить количество символов в числе (стащили с киберфорума)
        }

        static string PrintWhiteSpace(int number) //понять сколько пробелов напечатать, делал это чтобы не засорять Console.WriteLine
        {
            var spaceString = "";

            for (int i = 0; i < number; i++)
            {
                spaceString += " ";
            }

            return spaceString;
        }
    }
}
